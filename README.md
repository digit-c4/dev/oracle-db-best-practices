# oracle-db-best-practices

## Install Oracle DB on Debian
There is no known package for Oracle DB client on Debian.
The alternative is to download the oracle linux distribution client, and to
transform it into a `.deb` installable package. [Alien](https://wiki.debian.org/Alien) can be used to perform
that translation
```shell
# Link to last version found on
# https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html
curl https://download.oracle.com/otn_software/linux/instantclient/1922000/oracle-instantclient19.22-basic-19.22.0.0.0-1.x86_64.rpm --output instantclient-basic-linux.rpm
sudo apt-get install alien
sudo alien -d instantclient-basic-linux.rpm
sudo dpkg -i oracle-instantclient19.22-basic_19.22.0.0.0-2_amd64.deb
```

> ⚠️ WARNING ⚠️
> 
> Using Alien to translate an Oracle linux package into a Debian linux package is at your own risks

in order to make the client working with legacy script, it may be necessary to force those environments variable with this default setting :

```bash
$ export ORACLE_BASE=/usr/lib/oracle/19.22
$ export ORACLE_HOME=/usr/lib/oracle/19.22/client64
$ export LD_LIBRARY_PATH=/usr/lib/oracle/19.22/client64/lib
$ export TNS_ADMIN="/$PROJECT_ROOT/deployment/ora"
```

## ORA config
Configuration to Oracle database stands in (aweful) `.ora` files located in `/$PROJECT_ROOT/deployment/ora`.
Our standard setup requires three files:

- ldap.ora
- sqlnet.ora
- tnsnames.ora

### ldap.ora 
See the [example file](deployment/ora/ldap.ora.example), including three (3) variable :

- `DEFAULT_ADMIN_CONTEXT`: specifies the default directory for the creation, modification, or search of the connect identifiers, constructed by the syntax "dc=<domain_part_1>,dc=<domain_part_2>(...)".
- `DIRECTORY_SERVERS`: a directory usage parameter and it lists the host names and port number of the primary and alternate LDAP directory servers.
Its syntax is <HOSTNAME>:<PRIMARY_LDAP_DIRECTORY>:<ALTERNATE_LDAP_DIRECTORY>
- `DIRECTORY_SERVER_TYPE`: type of previously described server (IN DIRECTORY_SERVERS), generally represented by a single string.

### sqlnet.ora
See the [example file](deployment/ora/sqlnet.ora.example), a plain-text configuration file that contains the information 
(like tracing options, encryption, route of connections, external naming parameters etc.) on how both Oracle server and 
Oracle client have to use Oracle Net capabilities for networked database access.

### tnsnames.ora
See the [example file](deployment/ora/tnsnames.ora.example), that contains network service names mapped to connect descriptors for the local naming method, or net service names mapped to listener protocol addresses.

## Connect with Python
Connect to database through [SQLALCHEMY](https://www.sqlalchemy.org/) python module
```python
import os
import configparser

from sqlalchemy import engine, exc, text
from sqlalchemy.pool import QueuePool
from dotenv import load_dotenv, find_dotenv


_ = load_dotenv(find_dotenv()) # Info : Read the local .env file

tablename = os.getenv("TABLENAME")
history_table = os.getenv("HISTORY_TABLE")
combined_table = os.getenv("COMBINED_TABLE")
db_user = os.getenv("DB_USER")
db_pwd = os.getenv("DB_PWD")
db_sid = os.getenv("DB_SID")


def _db_gen_url():
    """
    Generates the database url for sql alchemy. This is used as Oracle databases url can be a bit tedious.
    @param section_name: The section in the ini file whith the given parameters.
    @return the database url.
    """
    if not db_user or not db_pwd or not db_sid:
        raise ValueError("Missing elements in section detected. You must at least fill USER, PASSWORD & SID.")

    return 'oracle+cx_oracle://{db_user}:{db_pwd}@{db_sid}'.format(db_user=db_user,
                                                                    db_pwd=db_pwd,
                                                                    db_sid=db_sid)


myengine = engine.create_engine(_db_gen_url(),
                                echo=False,
                                poolclass=QueuePool,
                                implicit_returning=False,
                                pool_size=10,
                                pool_timeout=180,
                                max_overflow=10,
                                pool_pre_ping=True)


def _db_get():
    """
    Get an open connection object or creates one if none exists
    @return sqlalchemy connection object
    """
    try:
        return myengine.connect()
    except Exception as e:
        print('Script could not connect engine. %s' % (str(e)))
        # trying again in an optimist thinking.
        raise


_db_get()
```

If this piece of code works when running with your virtual environment activated, then you can start quering your Oracle instance in Python !
